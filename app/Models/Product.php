<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        return $this->belongsToMany(Unit::class,'product_unit')->withPivot("amount");
    }

    
    public function getTotalQuantityAttribute()
    {
        return $this->units
               ->reduce(fn($carry, $item) => $carry + $item->modifier * $item->pivot->amount);
    } 

  
    public function getImagePathAttribute()
    {
        return  $this->images()->first()->path ?? '';
    }


    public function images(){
        return $this->morphOne(Image::class,'imageable');
    }
}
