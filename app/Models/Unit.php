<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Unit extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'modifier',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'product_unit')->withPivot("amount");

    }
}
